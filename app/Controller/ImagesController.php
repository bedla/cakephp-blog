<?php
App::uses('AppController', 'Controller');

/**
 * Images Controller
 *
 * @property Image $Image
 */
class ImagesController extends AppController {
    public $helpers = array('Html','Js', 'Fancybox.Fancybox','TinyMCE.TinyMCE');


    public function isAuthorized($user) {
    // The owner of a image and admin can edit and delete it
    if (in_array($this->action, array('edit', 'delete'))) {
        $imageId = $this->request->params['pass'][0];
        if ($this->Image->isOwnedBy($imageId, $user['id'])) {
            return true;
        }
    }
    if ($this->action === 'add' && $user['Role']['name']=='user' ) {
        return false;
    }
        if ($this->action === 'add' && $user['Role']['name']!='user' ) {
        return true;
    }
          if(in_array($user['Role']['name'],array('admin','moderator'))){return true;}
    return parent::isAuthorized($user);
}
    
    
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Image->recursive = 0;
		$this->set('images', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Image->exists($id)) {
			throw new NotFoundException(__('Invalid image'));
		}
		$options = array('conditions' => array('Image.' . $this->Image->primaryKey => $id));
		$this->set('image', $this->Image->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Image->create();
			if ($this->uploadFile() && $this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'));
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		}
		$users = $this->Image->User->find('list');
		$this->set(compact('users'));
	}
        
        
        public function uploadFile() {
            debug($this->request->data);
  $file = $this->request->data['Image']['file'];
  if ($file['error'] === UPLOAD_ERR_OK) {
    $id = String::uuid();

    if (move_uploaded_file($file['tmp_name'], APP.'webroot'.DS.'uploads'.DS.$id."-".$file['name'])) {
      $img=new Imagick(APP.'webroot'.DS.'uploads'.DS.$id."-".$file['name']);
      $img->thumbnailImage(200, 0); 
      if($img->writeimage(APP.'webroot'.DS.'uploads'.DS.'thumb-'.$id."-".$file['name'])){
        $this->request->data['Image']['url'] = $id."-".$file['name'];
      $this->request->data['Image']['user_id'] = $this->Auth->user('id');
      $this->request->data['Image']['name'] = $file['name'];
     // $this->data['Upload']['filesize'] = $file['size'];
      $this->request->data['Image']['type'] = $file['type'];
      return true;
    }}
  }
  return false;
}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Image->exists($id)) {
			throw new NotFoundException(__('Invalid image'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Image.' . $this->Image->primaryKey => $id));
			$this->request->data = $this->Image->find('first', $options);
		}
		$users = $this->Image->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Image->delete()) {
			$this->Session->setFlash(__('Image deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Image was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


}
