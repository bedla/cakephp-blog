<?php
class SitemapsController extends AppController {

    var $name = 'Sitemaps';
    var $uses = array('Article');
    var $helpers = array('Time');
    var $components = array('RequestHandler');

  public function index() {
        $this->layout = "empty";
        $this->set('articles', $this->Article->find('visible'));
    }

}
?>