<?php
App::uses('AppController', 'Controller');
/**
 * ArticlesKeywords Controller
 *
 * @property ArticlesKeyword $ArticlesKeyword
 */
class ArticlesKeywordsController extends AppController {

        public function element(){
         $data=  $this->ArticlesKeyword->query("SELECT a.keyword_id, COUNT( a.keyword_id ) AS count, b.seo_link, b.name
                                                FROM articles_keywords a, keywords b
                                                WHERE b.id = a.keyword_id
                                                GROUP BY a.keyword_id
                                                LIMIT 0 , 20");
         $count=  $this->ArticlesKeyword->query("SELECT COUNT(*) as count FROM articles_keywords");

         $return['countAll']=$count;
        $return['keywords']=$data;
        return $return;

        /*
            $data = $this->ArticlesKeyword->find('all',array(
                'fields'=>array(
                    'Keyword.name','Keyword.seo_link','count(Article.id) AS count',),
                'group' => 'Keyword.name',
                'limit'=>'10'));
            $count = $this->ArticlesKeyword->find('count');
            $return['countAll']=$count;
        $return['keywords']=$data;
            if(isset($this->params['requested']))
	
                {
        return $return;
	}        */   
       }

}
