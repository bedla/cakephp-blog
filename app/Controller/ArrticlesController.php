    <?php
App::uses('AppController', 'Controller');
/**
 * Articles Controller
 *
 * @property Article $Article
 */
class ArrticlesController extends AppController {
var $components=array("Email","Session");

    var $helpers = array('Time','TinyMCE.TinyMCE','Html','Js',"Form","Session");

public function isAuthorized($user) {
    debug($user);
    return true;
    if ($this->action== 'user') {
    return true;
    
    }
    if($user['Role']['name']=='admin'){
        return true;
        
    }
   // The owner of a post and admin can edit and delete it
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = $this->request->params['pass'][0];
        
        if ($this->Arrticle->isOwnedBy($postId, $user['id'])) {

            return true;
        }
    }
    //if(in_array($this->action,array('index','view','user')))return true;
    if ($this->action === 'add' && $user['Role']['name']=='user' ) { //user nesmi pridavat
        return false;
    }
        if ($this->action === 'add' && $user['Role']['name']!='user' ) {
        return true;
    }

    return parent::isAuthorized($user);
}
  
/**
 * index method
 *
 * @return void
 */
	public function index() {
        $this->set('articles', $this->Arrticle->find('visible'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($permalink = null) {
            $article=  $this->Arrticle->findBySeoLink($permalink);
            if (!$article) {
			throw new NotFoundException(__('Invalid article'));
		}
		$this->set('article', $article);
   
                }

/**
 * add method
 *
 * @return void
 */
      	public function user() {
                    $user=$this->Auth->user();
                    
                    $articles = $this->Arrticle->findAllByUserId($user["id"]);
  
                    $this->set('no_article', false);
                    if(!$articles){
                    $this->Session->setFlash(__('Nemáte žádné komentáře'));
                    $this->set('no_article', true);
                }
		$this->set('articles', $articles);
	}
                
                public function add() {
		if ($this->request->is('post')) {
			$this->Arrticle->create();
			if ($this->Arrticle->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
		$users = $this->Arrticle->User->find('list');

		$categories = $this->Arrticle->Category->find('list');
		$keywords = $this->Arrticle->Keyword->find('list');
		$this->set(compact('users', 'categories', 'keywords'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Arrticle->exists($id)) {
			throw new NotFoundException(__('Invalid article'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Arrticle->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Article.' . $this->Arrticle->primaryKey => $id));
			$this->request->data = $this->Arrticle->find('first', $options);
		}
		$users = $this->Arrticle->User->find('list');
		$categories = $this->Arrticle->Category->find('list');
		$keywords = $this->Arrticle->Keyword->find('list');
		$this->set(compact('users', 'categories', 'keywords'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Arrticle->id = $id;
		if (!$this->Arrticle->exists()) {
			throw new NotFoundException(__('Invalid article'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Arrticle->delete()) {
			$this->Session->setFlash(__('Article deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Article was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


}
