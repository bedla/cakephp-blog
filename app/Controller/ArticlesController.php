<?php

App::uses('AppController', 'Controller');

/**
 * Articles Controller
 *
 * @property Article $Article
 */
class ArticlesController extends AppController {

    var $components = array("Email", "Session", "RequestHandler");
    var $helpers = array('Time', 'TinyMCE.TinyMCE', 'Html', 'Js', "Form", "Session", "Text");

    public function isAuthorized($user) {

        if ($this->action == 'user') {
            return true;
        }
        if ($user['Role']['name'] == 'admin') {
            return true;
        }
        // The owner of a post and admin can edit and delete it
        //if(in_array($this->action,array('index','view','user')))return true;
        if ($this->action === 'add' && $user['Role']['name'] == 'user') { //user nesmi pridavat
            return false;
        }
        if ($this->action === 'add' && $user['Role']['name'] != 'user') {

            return true;
        }
        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = $this->request->params['pass'][0];

            if (!$this->Article->isOwnedBy($postId, $user['id'])) {

                return false;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if ($this->RequestHandler->isRss() ) {
            $posts = $this->Article->find('visible', array('limit' => 20));
            return $this->set(compact('posts'));
        }
        $this->set('articles', $this->Article->find('visible'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($permalink = null) {
        $article = $this->Article->findBySeoLink($permalink);
        if (!$article) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->set('article', $article);
    }

    /**
     * add method
     *
     * @return void
     */
    public function user() {
        $user = $this->Auth->user();

        $articles = $this->Article->findAllByUserId($user["id"]);

        $this->set('no_article', false);
        if (!$articles) {
            $this->Session->setFlash(__('Nemáte žádné komentáře'));
            $this->set('no_article', true);
        }
        $this->set('articles', $articles);
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Article->create();
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }
        $users = $this->Article->User->find('list');

        $categories = $this->Article->Category->find('list');
        $keywords = $this->Article->Keyword->find('list');
        $this->set(compact('users', 'categories', 'keywords'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
            $this->request->data = $this->Article->find('first', $options);
        }
        $users = $this->Article->User->find('list');
        $categories = $this->Article->Category->find('list');
        $keywords = $this->Article->Keyword->find('list');
        $this->set(compact('users', 'categories', 'keywords'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Article->delete()) {
            $this->Session->setFlash(__('Article deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Article was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
