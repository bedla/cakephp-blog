<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 */
class CategoriesController extends AppController {
var $helpers = array('Time','TinyMCE.TinyMCE','Html','Js'/*, 'Fancybox.Fancybox'*/);

public function isAuthorized($user) {
    if (in_array($this->action ,array('add','edit')) && $user['Role']['name']=='admin' ) { //pridavat a editovat kategorie muze jen admin
        return true;
    }
    return parent::isAuthorized($user);
}

        /**
 * index method
 *
 * @return void
 */
      	
        public function getMenu() {
		$categories = $this->Category->find('all', array('fields'=>array('seo_link', 'name'),
							   'recursive'=>0,
							   'order'=>array('name desc')
							   ));
                if(isset($this->params['requested']))
	{
		return $categories;
	}
 
	$this->set('categories', $categories);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($permalink = null) {
            $aid=$this->Category->findBySeoLink($permalink);
            $articles = $this->Category->Article->findAllByCategoryId($aid['Category']['id']);
		
            if (!$articles) {
			throw new NotFoundException(__('Invalid category or no article'));
		}

                $this->set('articles', $articles);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'));
				//$this->redirect(array('controller'=>null,'action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('Category deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Category was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


}
