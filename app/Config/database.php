<?php
class DATABASE_CONFIG {

        	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '',
		'database' => 'cms_final',
		'prefix' => '',
		'encoding' => 'utf8'
	);
}
