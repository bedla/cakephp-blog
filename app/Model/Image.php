<?php
App::uses('AppModel', 'Model');
/**
 * Image Model
 *
 * @property User $User
 */
class Image extends AppModel {
 var $name = 'Image';
     
  public function beforeSave($options = array()) {
        parent::beforeSave();
        if($user['Role']['name']!="admin"){
            $this->data["Article"]["user_id"]=$user["id"];
        };     
        return true;
    }
 
 public function isOwnedBy($image, $user) {
    return $this->field('id', array('id' => $image, 'user_id' => $user)) === $image;
 }
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
 
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
