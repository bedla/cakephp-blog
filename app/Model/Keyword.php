<?php
App::uses('AppModel', 'Model');
/**
 * Keyword Model
 *
 */
class Keyword extends AppModel {

        public function beforeSave($options = array()) {
        parent::beforeSave();
        if($this->data["Keyword"]["seo_link"]==NULL){
        $slug=Inflector::slug($this->data["Keyword"]["name"]);
        $count=count($this->findBySeoLink($slug));
        if($count==0){
            $this->data["Keyword"]["seo_link"]= Inflector::slug($this->data["Keyword"]["name"]);
        }
        else
        {
            $this->data["Keyword"]["seo_link"]= Inflector::slug($this->data["Keyword"]["name"])."-".$count;
        }
        
        }
        return true;
    }
/**

        /**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
