<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Article $Article
 */
class Category extends AppModel {

        public function beforeSave($options = array()) {
        parent::beforeSave();
        $slug=Inflector::slug($this->data["Category"]["name"]);
        $count=count($this->findBySeoLink($slug));
        if($count==0){
            $this->data["Category"]["seo_link"]= Inflector::slug($this->data["Category"]["name"]);
        }
        else
        {
            $this->data["Category"]["seo_link"]= Inflector::slug($this->data["Category"]["name"])."-".$count;
        }
        return true;
    }
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'parent' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'isDefault' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
