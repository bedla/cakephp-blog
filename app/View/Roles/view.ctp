<div class="roles view">
<h2><?php  echo __('Role'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($role['Role']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($role['Role']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddArticle'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddArticle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddCategory'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddCategory']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddComment'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddComment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddFile'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddFile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddImage'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddImage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddKeyword'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddKeyword']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddRole'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddRole']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanAddUser'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canAddUser']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditArticle'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditArticle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditCategories'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditCategories']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditComments'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditComments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditFile'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditFile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditImage'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditImage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditKeyword'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditKeyword']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditRole'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditRole']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanEditUser'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canEditUser']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteArticle'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteArticle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteCategory'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteCategory']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteComment'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteComment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteFile'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteFile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteImage'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteImage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteKeyword'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteKeyword']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteRole'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteRole']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CanDeleteUser'); ?></dt>
		<dd>
			<?php echo h($role['Role']['canDeleteUser']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Role'), array('action' => 'edit', $role['Role']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Role'), array('action' => 'delete', $role['Role']['id']), null, __('Are you sure you want to delete # %s?', $role['Role']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($role['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Role Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($role['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['role_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
