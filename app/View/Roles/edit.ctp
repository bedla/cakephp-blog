<div class="roles form">
<?php echo $this->Form->create('Role'); ?>
	<fieldset>
		<legend><?php echo __('Edit Role'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('canAddArticle');
		echo $this->Form->input('canAddCategory');
		echo $this->Form->input('canAddComment');
		echo $this->Form->input('canAddFile');
		echo $this->Form->input('canAddImage');
		echo $this->Form->input('canAddKeyword');
		echo $this->Form->input('canAddRole');
		echo $this->Form->input('canAddUser');
		echo $this->Form->input('canEditArticle');
		echo $this->Form->input('canEditCategories');
		echo $this->Form->input('canEditComments');
		echo $this->Form->input('canEditFile');
		echo $this->Form->input('canEditImage');
		echo $this->Form->input('canEditKeyword');
		echo $this->Form->input('canEditRole');
		echo $this->Form->input('canEditUser');
		echo $this->Form->input('canDeleteArticle');
		echo $this->Form->input('canDeleteCategory');
		echo $this->Form->input('canDeleteComment');
		echo $this->Form->input('canDeleteFile');
		echo $this->Form->input('canDeleteImage');
		echo $this->Form->input('canDeleteKeyword');
		echo $this->Form->input('canDeleteRole');
		echo $this->Form->input('canDeleteUser');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Role.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Role.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Roles'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
