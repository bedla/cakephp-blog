<div class="roles index">
	<h2><?php echo __('Roles'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddArticle'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddCategory'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddComment'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddFile'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddImage'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddKeyword'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddRole'); ?></th>
			<th><?php echo $this->Paginator->sort('canAddUser'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditArticle'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditCategories'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditComments'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditFile'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditImage'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditKeyword'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditRole'); ?></th>
			<th><?php echo $this->Paginator->sort('canEditUser'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteArticle'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteCategory'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteComment'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteFile'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteImage'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteKeyword'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteRole'); ?></th>
			<th><?php echo $this->Paginator->sort('canDeleteUser'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($roles as $role): ?>
	<tr>
		<td><?php echo h($role['Role']['id']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['name']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddArticle']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddCategory']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddComment']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddFile']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddImage']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddKeyword']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddRole']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canAddUser']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditArticle']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditCategories']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditComments']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditFile']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditImage']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditKeyword']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditRole']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canEditUser']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteArticle']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteCategory']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteComment']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteFile']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteImage']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteKeyword']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteRole']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['canDeleteUser']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $role['Role']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $role['Role']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $role['Role']['id']), null, __('Are you sure you want to delete # %s?', $role['Role']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Role'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
