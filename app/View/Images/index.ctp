<div class="images index">

    
	<?php foreach ($images as $image): ?>
<?php    $src3 = "/uploads/".$image['Image']['url'];
		
		$this->Fancybox->setProperties( array( 
			 'class' => 'fancybox3',
			 'className' => 'fancybox.image',
			 'title'=>$this->Html->clean($image['Image']['name']),
			 'rel' => 'gallery1'
			  )
		);
		
		$this->Fancybox->setPreviewContent($this->Html->image("/uploads/thumb-".$image['Image']['url']));
		
		$this->Fancybox->setMainContent($src3); 

		echo $this->Fancybox->output();	?>
    <?php if(in_array($current_user['Role']['name'],array('admin','moderator')) || $current_user['username'] ==$image['User']['username']): ?>
    			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $image['Image']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $image['Image']['id']), null, __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?>
        <?php endif; ?>
<?php endforeach; ?>
</div>