<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Blog2blog');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.novy');
                echo $this->Html->css('print.css',null, $options=array("media"=>"print"));

		echo $this->fetch('meta');
		echo $this->fetch('css');
                echo $this->Html->script(array('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'));
		echo $this->fetch('script');
	?>
<link href="http://fonts.googleapis.com/css?family=Arvo" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- start header -->
<div id="header">
	<div id="logo">
		<h1><?php echo $this->Html->link($cakeDescription, "/"); ?></h1>
	</div>
	<div id="menu">
            <?php echo $this->element('topMenu');  ?>
        </div>
</div>
<hr />
<!-- end header -->
<!-- start page -->
<div id="wrapper">
	<div id="page">
		<!-- start content -->
		<div id="content">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<!-- end content -->
		<!-- start sidebar -->
		<div id="sidebar">
			<ul>

                            <li id="categories">
					<h2>Categories</h2>
                            <?php echo $this->element('categoriesMenu');  ?>
				</li>
                             <li id="social">
					<h2>Social</h2>
                            <?php echo $this->element('social');  ?>
				</li>
                            
			<?php if ($logged_in): ?>
                            <li id="actions">
					<h2>Akce</h2>
					<?php echo $this->element('actionsMenu');  ?>
				</li>	
                            <?php endif; ?>	
                            <li id="calendar">
					<h2>Keywords</h2>
                                        <?php echo $this->element('keywords');  ?>
				</li>
				
			</ul>
		</div>
		<!-- end sidebar -->
		<br style="clear: both;" />
	</div>
</div>
<!-- end page -->
<!-- start footer -->
<div id="footer">
<p id="legal">			
    <?php echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('alt' => $cakeDescription)),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false)
				);
			?>
</p>
<!-- end footer -->
</div>
</body>
</html>
