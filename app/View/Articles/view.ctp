<?php $this->TinyMCE->editor(array('theme' => 'advanced', 'mode' => 'textareas')); ?>
<div class="articles index">
	<h2><?php echo __('Articles'); ?></h2>
        <div class="post">        
	<h2 class="title"><?php echo h($article['Article']['title']); ?></h2>
	<p class="meta">
          <p class="date">
              <?php echo $this->Time->format('M', h($article['Article']['created'])); ?>
              <b>
                   <?php echo $this->Time->format('d', h($article['Article']['created'])); ?>
              </b>
          </p>
                Posted <?php echo $this->Html->link($article['User']['username'], array('controller' => 'users', 'action' => 'view', $article['User']['id'])); ?> 
                               <?php if(in_array($current_user['Role']['name'],array('admin','moderator'))
                                ||$current_user['id']==$article['Article']['user_id']): 
                echo $this->Html->link(__('Edit'), array('action' => 'edit', $article['Article']['id'])); 
                echo $this->Form->postLink(__(' Delete'), array('action' => 'delete', $article['Article']['id']), null, __('Are you sure you want to delete # %s?', $article['Article']['id'])); 
                endif;
                ?> | <?php echo $this->Html->link($article['Category']['name'], array('controller' => 'categories', 'action' => 'view', $article['Category']['id'])); ?>, | 
                        <?php
        if (count($article['Comment']) <2&&count($article['Comment']) >0) {
            echo count($article['Comment']) . " komentář";
        } elseif (count($article['Comment']) <5) {
            echo count($article['Comment']) . " komentáře";
        } elseif (count($article['Comment']) > 4) {
            echo count($article['Comment']) . " komentářů";
        } else {
            echo "Bez komentáře";
        }
        ?>

        </p>
        <?php echo $this->Html->clean($article['Article']['content']); ?>        
  	</div>
        <p class="noprint">New comment</p>
	<div class="noprint">
            <?php
        echo $this->Form->create('Comment', array('url' => '/comments/add'));
		echo $this->Form->input('title');
        echo $this->Form->input('CommentText',array('type'=>'textarea'));

                echo $this->Form->input('article_id', array( 'value' => $article['Article']['id']  , 'type' => 'hidden') ); 
        
		$i = 0; ?>
        <?php echo $this->Form->end(__('Submit')); ?>
        </div>
		<?php foreach ($article['Comment'] as $comment): ?>

			<h4><?php echo $this->Html->clean($comment['title']); ?></h4>
                        			<?php if(in_array($current_user['Role']['name'],array('admin','moderator'))
                                ||$current_user['id']==$comment['user_id']
                                ||$current_user['id']==$article['User']['id']): ?>

				<?php echo $this->Html->link(__('View | '), array('controller' => 'comments', 'action' => 'view', $comment['id'])); ?>
				<?php echo $this->Html->link(__('Edit | '), array('controller' => 'comments', 'action' => 'edit', $comment['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete | '), array('controller' => 'comments', 'action' => 'delete', $comment['id']), null, __('Are you sure you want to delete # %s?', $comment['id'])); ?>
			
                         <?php endif; ?>
			<p><?php echo $this->Html->clean($comment['text']); ?></p>

		
	<?php endforeach;?>
</div>

