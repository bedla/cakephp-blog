	<?php foreach ($articles as $article): ?>
        <div class="post">        
	<h2 class="title"><?php 
        if($article['Article']['visible']==false){echo "[KONCEPT] ";}
        echo $this->Html->link(h($article['Article']['title']), array('action' => 'view', $article['Article']['seo_link'])); ?></h2>
	<p class="meta">
            <small>
          <p class="date">
              <?php echo $this->Time->format('M', h($article['Article']['created'])); ?>
              <b>
                   <?php echo $this->Time->format('d', h($article['Article']['created'])); ?>
              </b>
          </p>
                Posted <?php echo $this->Html->link($article['User']['username'], array('controller' => 'users', 'action' => 'view', $article['User']['id'])); ?> 
               <?php if(in_array($current_user['Role']['name'],array('admin','moderator'))
                                ||$current_user['id']==$article['Article']['user_id']): 
                echo $this->Html->link(__('Edit'), array('action' => 'edit', $article['Article']['id'])); 
                echo $this->Form->postLink(__(' Delete'), array('action' => 'delete', $article['Article']['id']), null, __('Are you sure you want to delete # %s?', $article['Article']['id'])); 
                endif;
                ?> Filed under <?php echo $this->Html->link($article['Category']['name'], array('controller' => 'categories', 'action' => 'view', $article['Category']['seo_link'])); ?>, | 
                <?php       if(count($article['Comment'])>0){ echo count($article['Comment'])." komentář";} 
                        elseif(count($article['Comment'])>1){ echo count($article['Comment'])." komentáře";}
                        elseif(count($article['Comment'])>4){ echo count($article['Comment'])." komentářů";}
                        else { echo "Bez komentáře"; }
                        ?>
            </small>
        </p>
        <?php echo String::truncate($this->Html->clean($article['Article']['content']),250); ?>        
  	</div>
<?php endforeach; ?>


