<?php
App::uses('Sanitize', 'Utility');

$this->set('channelData', array(
    'title' => __("Nejnovější články"),
    'link' => $this->Html->url('/', true),
    'description' => __("Nejnovější články."),
    'language' => 'cs-cz'
));


foreach ($posts as $post) {
    $postTime = strtotime($post['Article']['created']);

    $postLink = array(
        'controller' => 'articles',
        'action' => 'view',
        'year' => date('Y', $postTime),
        'month' => date('m', $postTime),
        'day' => date('d', $postTime),
        $post['Article']['seo_link']
    );

    // This is the part where we clean the body text for output as the description
    // of the rss item, this needs to have only text to make sure the feed validates
    $bodyText = preg_replace('=\(.*?\)=is', '', $post['Article']['content']);
    $bodyText = $this->Text->stripLinks($bodyText);
    $bodyText = Sanitize::stripAll($bodyText);
    $bodyText = $this->Text->truncate($bodyText, 400, array(
        'ending' => '...',
        'exact'  => true,
        'html'   => true,
    ));

    echo  $this->Rss->item(array(), array(
        'title' => $post['Article']['title'],
        'link' => $postLink,
        'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
        'description' => $bodyText,
        'pubDate' => $post['Article']['created']
    ));
}

?>