<?php $this->TinyMCE->editor(array('theme' => 'advanced', 'mode' => 'textareas')); ?>
<div class="articles form">
<?php echo $this->Form->create('Article'); ?>
	<fieldset>
		<legend><?php echo __('Add Article'); ?></legend>
	<?php
		echo $this->Form->input('content');
		echo $this->Form->input('title');
		echo $this->Form->input('visible');
                                if($current_user['Role']['name']=='admin'){
                echo $this->Form->input('user_id');}
		echo $this->Form->input('category_id');
		echo $this->Form->input('Keyword');
                //echo $this->Form->input('new_Keyword',array('type'=>'text','label'=>'Další klíčová slova oddělte čásrkou'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
