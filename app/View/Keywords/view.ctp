<div class="keywords view">
<h2><?php  echo __('Keyword'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seo Link'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['seo_link']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php if($current_user['Role']['name']=='admin'): ?>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Keyword'), array('action' => 'edit', $keyword['Keyword']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Keyword'), array('action' => 'delete', $keyword['Keyword']['id']), null, __('Are you sure you want to delete # %s?', $keyword['Keyword']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Keywords'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Keyword'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php endif; ?>
