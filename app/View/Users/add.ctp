<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('username');
                echo $this->Form->input('email');
		echo $this->Form->input('password', array('type'=>'password'));
                echo $this->Form->input('password_confirmation', array('type'=>'password'));
		if($current_user['Role']['name']=='admin'){
		echo $this->Form->input('role_id');
                }
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

