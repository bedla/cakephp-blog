<?php 
/* First step: get the latest posts, the URL should be like your_controller_name/method_name/params */

?>
<ul>

<?php if(in_array($current_user["Role"]["name"],array("user","writer","moderator","admin"))): //zobrazim vsem ?>
        <li>

   <?php echo $this->Html->link("Správa účtu", array('controller'=>'users','action' => 'edit', $current_user["id"]));
 ?>
    </li>
    <li>
        <?php echo $this->Html->link("Moje komentáře", array('controller'=>'comments','action' => 'user')); ?>
    </li>
    <?php endif; ?> 

<?php if(in_array($current_user["Role"]["name"],array("writer","moderator","admin"))): ?>
    <li>
        <?php echo $this->Html->link("Přidat článek", array('controller'=>'articles','action' => 'add')); ?>
    </li>
        <li>
        <?php echo $this->Html->link("Přidat klíčové slovo", array('controller'=>'keywords','action' => 'add')); ?>
    </li>
        <li>
        <?php echo $this->Html->link("Přidat obrázek", array('controller'=>'images','action' => 'add')); ?>
    </li>
    <li>
        <?php echo $this->Html->link("Moje články", array('controller'=>'articles','action' => 'user')); ?>
    </li>
     <?php endif; ?>
    <?php if(in_array($current_user["Role"]["name"],array("moderator","admin"))):?>
    <li>
         <?php echo $this->Html->link("Zobrazit uživatele", array('controller'=>'users','action' => 'index')); ?>
    </li>
        <li>
         <?php echo $this->Html->link("Spravovat články", array('controller'=>'articles','action' => 'index')); ?>
    </li>
    <?php endif; ?>
    <?php if(in_array($current_user["Role"]["name"],array("admin"))):?>
          <li>
         <?php echo $this->Html->link("Spravovat klíčová slova", array('controller'=>'keywords','action' => 'index')); ?>
    </li> 
    <li>
         <?php echo $this->Html->link("Přidat kategorii", array('controller'=>'categories','action' => 'add')); ?>
    </li>   
  
    <li>
        <?php echo $this->Html->link("Přidat uživatele", array('controller'=>'users','action' => 'add')); ?>
    </li>
    <?php endif; ?>
  
</ul>