<?php 
$keywords = $this->requestAction('ArticlesKeywords/element');

foreach ($keywords['keywords'] as $keyword): ?>
<p style="display: inline;font-size:<?php echo round($keyword[0]['count']/$keywords['countAll'][0][0]['count']*300, 0) ?>%;">
    <?php echo $this->Html->link(h($keyword['b']['name']."(".$keyword[0]['count'].") "), array('controller'=>'Keywords','action' => 'view', $keyword['b']['seo_link'])); ?>
</p>	
<?php 
endforeach; ?>        