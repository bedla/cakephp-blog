<?php 
/* First step: get the latest posts, the URL should be like your_controller_name/method_name/params */
$categories = $this->requestAction('Categories/getMenu');
?>
<ul>
<?php foreach($categories as $category): ?>
	<li><?php echo $this->Html->link($category['Category']['name'], array('controller'=>'categories', 'action'=>'view', $category['Category']['seo_link'])); ?>
        <?php if($current_user['Role']['name']=="admin"): ?>
            <?php echo $this->Html->link($this->Html->image('edit_icon.gif',array('fullBase'=>true)),
                array('controller' => 'categories','action' => 'edit', $category['Category']['id']),array('escape'=>false)); ?>
	<?php echo $this->Form->postLink($this->Html->image('delete_icon.gif',array('fullBase'=>true)),
                array('controller'=>'categories', 'action' => 'delete', $category['Category']['id']), array('escape'=>false), __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?>
        </li>
        
<?php 
            endif;
endforeach; ?>
</ul>