<ul>
	<li><?php echo $this->Html->link("Home", "/"); ?></li>
	<li><?php echo $this->Html->link("Gallery", array('controller' => 'images', 'action' => 'index')); ?></li>
	<li><?php echo $this->Html->link("About", "/pages/about"); ?></li>
	<li><?php echo $this->Html->link("Contact", "/pages/contact"); ?></li>
        <?php if($logged_in): ?>
        <li><?php echo $this->Html->link("Log out (".$current_user['username'].")", array('controller' => 'users', 'action' => 'logout')); ?></li>
        <?php endif; 
        if (!$logged_in):
        ?>
                <li><?php echo $this->Html->link("Log in", array('controller' => 'users', 'action' => 'login')); ?></li>
        <?php endif; ?>
</ul>