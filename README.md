# CMS – Redakční systém
http://janbednar.eu
Autor: Jan Bednář <mail@janbednar.eu>

## Aplikace
* Aplikace bude sloužit pro tvorbu miniwebů, které se používají pro podporu návštěvnosti hlavního webu.
* Role použité v aplikaci jsou: admin, moderator, writer, user
* Uživatel si může poslat na mail odkaz pro resetování hesla
* Hesla v databázi jsou automaticky šifrována
* Editace a přidávání dat řešeno pomocí validovaných formulářů
* Aplikace je chráněna proti SQL injection a XSS
* Při nahrání obrázku do galerie se automaticky vygeneruje i miniatura
## Použitá technologie
* Aplikace pracuje na frameworku CakePHP ve verzi 2.3.1 s podporou pro php 5.3 a vyšší
* WYSIWYG editor (TinyMCE) a prohlížeč obrázků (Fancybox)
* Kódování v jazyce php, html5, css
* Data jsou uložena v databázi MySQL
* Knihovna ImageMagick pro tvorbu miniatur v galerii
## Možnosti dalšího rozvoje
* Zvýrazňování klíčových slov v textu přispěvku společně s odkazem, pro lepší prolinkování webu.
* Implementovat vyhledávání
* Obrázky v galerii třídit podle kategorií. V současné době se pod záložkou galerie zobrazí všechny
* Rozumnější počítání velikosti klíčového slova v elementu Keywords
* Vyladit routování, pro sekce, kde to má z hlediska SEO smysl.
* Možnost nastavovat základní nastavení webu, například použité elementy v layoutu, titulek, description, logo, v administraci.
* Implementovat nový element pro zobrazování Google reklam
* Přidat nový element, který bude zobrazovat fráze, ze kterých návštěvníci z vyhledávačů, přicházejí nejčastěji + další nástroje pro vylepšení cílení.

## Defaultní uživatelé a role
| Username  | Password  | Typ účtu  | Práva                                                                                               |
|-----------|-----------|-----------|-----------------------------------------------------------------------------------------------------|
| user      | user      | user      | Zobrazovat články, přidávat komentáře, editovat a mazat vlastní komentáře                           |
| writer    | writer    | writer    | Práva usera + možnost přidávat články, editovat a mazat vlastní články                              |
| moderator | moderator | moderator | Práva writera + editace a mazání všech komentářů                                                    |
| admin     | admin     | admin     | Kompletní přistup ke všem funkcím systému, včetně správy komentářů , správy uživatelů a jejich rolí |