-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Počítač: wm29.wedos.net:3306
-- Vygenerováno: Pon 06. kvě 2013, 15:15
-- Verze MySQL: 5.5.30
-- Verze PHP: 5.4.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `d36475_tnpw2`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_link` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `content` text COLLATE utf8_czech_ci,
  `created` datetime NOT NULL,
  `title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=26 ;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`id`, `seo_link`, `content`, `created`, `title`, `visible`, `user_id`, `category_id`, `modified`) VALUES
(15, 'XSS', '<p>v&scaron;echny vstupy jsou o&scaron;etřeny proti XSS</p>', '2013-04-29 02:37:14', 'XSS', 1, 17, 7, '2013-04-29 14:14:34'),
(16, 'lipsum', '<p>čl&aacute;nek se při v&yacute;pisu ořez&aacute;va na 250 znaků</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper dignissim tellus, in bibendum nisl pharetra non. Praesent iaculis fringilla enim vulputate ornare. Etiam adipiscing elit vitae lectus sagittis ac luctus justo varius. Mauris ut elit arcu, a tincidunt felis. Vestibulum et ligula at lectus rhoncus sollicitudin vitae non turpis. Maecenas aliquam elit sed enim malesuada malesuada. Nunc accumsan turpis pretium mi scelerisque et congue elit semper. Mauris tincidunt nisi vel velit scelerisque interdum. Fusce massa orci, dictum quis cursus nec, varius vitae arcu. Fusce molestie risus vel lorem imperdiet vestibulum. Praesent suscipit metus sed magna adipiscing tincidunt. Praesent iaculis mi id mi facilisis placerat. Proin et auctor massa.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nunc ornare semper leo vel imperdiet. Maecenas risus dolor, malesuada id dignissim eu, consequat volutpat orci. Nullam eleifend, nisl eu suscipit gravida, tellus metus tincidunt est, nec aliquet ligula nisi eu erat. Vivamus bibendum tellus non nunc dapibus mattis. Curabitur ultrices ligula sed est ullamcorper rhoncus tincidunt felis consectetur. Etiam ac purus tortor, quis mattis purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur sodales tincidunt felis eget luctus. Donec sed diam sed turpis commodo sagittis non et sapien. Nulla auctor egestas lacinia. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac arcu sit amet lectus dictum viverra. Curabitur pulvinar laoreet arcu et consequat. Nullam velit lorem, hendrerit non eleifend at, rutrum ac quam. Nam laoreet orci consequat augue consectetur a lacinia arcu sagittis. Praesent id mi odio, ac pulvinar lorem.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Donec mollis ullamcorper metus, sed vestibulum dui accumsan tempus. Maecenas condimentum libero quis massa fermentum fringilla. Donec sed tortor ac elit sagittis fermentum. Nam feugiat arcu sed odio luctus placerat vitae ut dolor. Phasellus id risus ac libero luctus varius. Sed vitae est interdum tortor elementum fermentum sit amet in elit. Morbi sollicitudin mi id massa euismod vulputate. Aenean scelerisque felis quis nisi sodales vitae euismod est condimentum. Pellentesque leo nisi, vestibulum a feugiat ut, venenatis ac massa.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Sed eu placerat libero. Vivamus semper luctus purus, eget tempus felis porta ut. Proin in elementum orci. Curabitur eu orci metus, pulvinar facilisis velit. In quis libero arcu, eu posuere nisi. Maecenas quam orci, varius eget rhoncus vel, volutpat volutpat nulla. Cras sem quam, tristique sed venenatis a, tincidunt ut quam. Fusce nulla turpis, ultricies faucibus vestibulum et, imperdiet ut mauris. Duis mattis tincidunt velit. Donec tempor tincidunt nunc quis iaculis.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Etiam vel nisl sit amet diam scelerisque aliquam. Cras dapibus hendrerit ullamcorper. Suspendisse convallis arcu non lorem fringilla vel molestie nunc scelerisque. Donec quis sapien eget metus lobortis faucibus. Nulla interdum cursus est in pretium. Phasellus tincidunt nunc diam. Suspendisse potenti. Morbi ac nunc a nulla rhoncus porttitor sed id est. Nulla accumsan posuere laoreet. Ut nec pellentesque dui.</p>', '2013-04-29 02:40:22', 'lipsum', 1, 17, 7, '2013-04-29 13:59:34'),
(17, 'Klicova_slova', '<p>Kl&iacute;čov&aacute; slova měn&iacute; v sidebaru velikost podle počtu čl&aacute;nků, kter&eacute; obsahuj&iacute;. Zat&iacute;m to nen&iacute; ide&aacute;ln&iacute; stav, protože velikost poč&iacute;t&aacute;m jako počet př&iacute;spěvků u kl&iacute;čov&eacute;ho slova/celkov&yacute; počet</p>', '2013-04-29 02:56:59', 'Klíčová slova', 1, 17, 7, '2013-04-29 03:05:38'),
(18, 'Neviditelny_clanek', '<p>Toto je neviditeln&yacute; čl&aacute;nek, nezobrazuje se zat&iacute;m na hlavn&iacute; str&aacute;nce ani ve v&yacute;pisu kategori&iacute;</p>', '2013-04-29 03:07:05', 'Neviditelný článek', 0, 17, 7, '2013-04-29 03:07:05'),
(19, 'clanek', '<p>čl&aacute;nek od uživatele "writer"</p>', '2013-04-29 03:39:34', 'clanek', 1, 18, 7, '2013-04-29 03:39:34'),
(20, 'Pristupove_udaje', '<p>testovac&iacute; uživatelsk&aacute; jm&eacute;na ZDE</p>\r\n<p><strong>Username:</strong> admin <strong>Password:</strong> admin <strong>Role:</strong> admin</p>\r\n<p><strong>Username:</strong> moderator <strong>Password:</strong> moderator <strong>Role:</strong> moderator</p>\r\n<p><strong>Username:</strong> writer <strong>Password:</strong> writer <strong>Role:</strong> writer</p>\r\n<p><strong>Username:</strong> user <strong>Password:</strong> user <strong>Role:</strong> user</p>', '2013-04-29 03:49:54', 'Přístupové údaje', 1, 18, 8, '2013-04-29 14:50:34'),
(21, 'bbb', '<p>aaaaaaaaaa</p>', '2013-04-29 08:34:33', 'bbb', 1, 19, 8, '2013-04-29 08:35:47'),
(22, 'test', '<p>Admin muze pridavat clanky za ostatni uzivatele</p>', '2013-04-29 14:13:13', 'test', 1, 20, 7, '2013-04-29 14:37:41'),
(23, 'bla', '<p>bla</p>', '2013-04-30 16:11:00', 'bla', 1, 18, 7, '2013-04-30 16:11:14'),
(24, 'dfhdf', '<p>fgfdf</p>', '2013-05-02 11:49:38', 'dfhdf', 1, 18, 7, '2013-05-02 11:49:50'),
(25, 'dfghd', '<p>dfgdf</p>', '2013-05-02 11:51:47', 'dfghd', 1, 17, 7, '2013-05-02 11:51:47');

-- --------------------------------------------------------

--
-- Struktura tabulky `articles_keywords`
--

CREATE TABLE IF NOT EXISTS `articles_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `keyword_id` (`keyword_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=29 ;

--
-- Vypisuji data pro tabulku `articles_keywords`
--

INSERT INTO `articles_keywords` (`id`, `article_id`, `keyword_id`) VALUES
(16, 17, 6),
(17, 18, 7),
(21, 22, 6),
(22, 20, 6),
(23, 23, 7),
(25, 25, 6),
(26, 25, 7),
(27, 25, 8),
(28, 25, 9);

-- --------------------------------------------------------

--
-- Struktura tabulky `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_link` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `categories`
--

INSERT INTO `categories` (`id`, `seo_link`, `description`, `name`) VALUES
(7, '1st', '<p><strong>prvn&iacute; kategorie</strong></p>', '1st'),
(8, '2nd', '<p><strong>druh&aacute; kategorie</strong></p>', '2nd');

-- --------------------------------------------------------

--
-- Struktura tabulky `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=29 ;

--
-- Vypisuji data pro tabulku `comments`
--

INSERT INTO `comments` (`id`, `text`, `title`, `user_id`, `article_id`, `created`, `modified`) VALUES
(22, '<script type="text/javascript">// <![CDATA[\r\nalert(''Toto je úspěšný XSS útok.'')\r\n// ]]></script>', 'XSS v komentáři', 18, 15, '2013-04-29 03:22:00', '2013-04-29 03:22:00'),
(23, '<p>Pod t&iacute;mto form&aacute;tovan&yacute;m textem je script, vykon&aacute; se?</p>\r\n<p>&nbsp;</p>\r\n<script type="text/javascript">// <![CDATA[\r\nalert(''Toto je úspěšný XSS útok.'')\r\n// ]]></script>', 'XSS2', 18, 15, '2013-04-29 03:23:01', '2013-04-29 03:23:01'),
(24, '', 'aaa</div>', 19, 16, '2013-04-29 08:33:22', '2013-04-29 08:33:58'),
(25, '<p>comment</p>', 'comment', 17, 19, '2013-04-29 22:52:56', '2013-04-29 22:52:56'),
(27, '<p>hhh</p>', 'hhh', 20, 15, '2013-05-02 11:48:49', '2013-05-02 11:48:49'),
(28, '<p><strong>test</strong></p>\r\n<script type="text/javascript">// <![CDATA[\r\nalert("ahoj");\r\n// ]]></script>', '<script>alert("ahoj");</script>', 21, 20, '2013-05-02 12:00:03', '2013-05-02 12:00:03');

-- --------------------------------------------------------

--
-- Struktura tabulky `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=530 ;

--
-- Vypisuji data pro tabulku `images`
--

INSERT INTO `images` (`id`, `url`, `type`, `description`, `name`, `user_id`, `created`, `modified`) VALUES
(528, '517dc362-52fc-4bab-ac1f-61c12e1c694e-CakePHP-Framework[1].jpg', 'image/jpeg', '<p>Při přid&aacute;n&iacute; obr&aacute;zku se automaticky generuje miniatura</p>', 'CakePHP-Framework[1].jpg', 17, '2013-04-29 02:48:35', '2013-04-29 02:52:27'),
(529, '5182388b-f9a0-4b20-9e32-49da2e1c694e-turtle.gif', 'image/gif', '<p><span style="text-decoration: line-through;"><strong>test</strong></span></p>', 'turtle.gif', 21, '2013-05-02 11:57:32', '2013-05-02 11:57:32');

-- --------------------------------------------------------

--
-- Struktura tabulky `keywords`
--

CREATE TABLE IF NOT EXISTS `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `seo_link` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=10 ;

--
-- Vypisuji data pro tabulku `keywords`
--

INSERT INTO `keywords` (`id`, `name`, `seo_link`, `created`, `modified`) VALUES
(6, 'jedna', 'jedna', '2013-04-29 02:09:50', '2013-04-29 02:09:50'),
(7, 'dva', 'dva', '2013-04-29 02:10:06', '2013-04-29 02:10:06'),
(8, 'tři', 'tři', '2013-04-29 02:10:13', '2013-04-29 02:10:13'),
(9, 'škola', 'skola', '2013-05-02 11:51:13', '2013-05-02 11:51:13');

-- --------------------------------------------------------

--
-- Struktura tabulky `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'moderator'),
(3, 'writer'),
(4, 'user');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_czech_ci NOT NULL COMMENT 'MD5 password',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `email` varchar(254) COLLATE utf8_czech_ci NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  `tokenhash` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=22 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role_id`, `email`, `modified`, `created`, `tokenhash`) VALUES
(17, 'admin', '4ee036abd8d2bd089282e6b23b4e0f144344eee0', 1, 'bedla.jedla@email.cz', '2013-04-29 02:04:34', '2013-04-29 02:04:34', ''),
(18, 'writer', '5dfe4311951a99654735363b5939cd473cd2cdaa', 3, 'writer@janbednar.eu', '2013-04-29 03:10:24', '2013-04-29 03:10:24', ''),
(19, 'moderator', 'fd4f6b89cde796bac8eefb6b86608cf8ab115b2e', 2, 'moderator@janbednar.eu', '2013-04-29 03:10:53', '2013-04-29 03:10:53', ''),
(20, 'user', '2a6476cb40239054f819a1c30a3c289ea6eb8970', 4, 'user@janbednar.eu', '2013-04-29 03:11:22', '2013-04-29 03:11:22', ''),
(21, 'writer2', 'd358b0aa7f073a82b4162acd026755f79cc435aa', 3, 'ff@jjj.cz', '2013-05-02 11:56:30', '2013-05-02 11:53:52', '');

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `FK_articles_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `FK_articles_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Omezení pro tabulku `articles_keywords`
--
ALTER TABLE `articles_keywords`
  ADD CONSTRAINT `FK_articles_keywords_articles` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  ADD CONSTRAINT `FK_articles_keywords_keywords` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`);

--
-- Omezení pro tabulku `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK_comments_articles` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  ADD CONSTRAINT `FK_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Omezení pro tabulku `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `FK_files_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Omezení pro tabulku `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_images_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
